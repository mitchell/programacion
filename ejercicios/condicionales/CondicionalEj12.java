package ejemplos;
import java.util.Scanner;
public class CondicionalEj12 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner lector = new Scanner(System.in);
		int year;
		String sino = "";
		System.out.println("Introduce a�o para comprobar si es bisiesto: ");
		year = lector.nextInt();
		if (year%4 == 0) {
			// si bisiesto
			sino = "si";
			if(year%100 == 0 && year%400 != 0) {
				// no bisiesto
				sino = "no";
			}
		} else {
			// no bisiesto
			sino = "no";
		}
		lector.close();
		System.out.println("El a�o "+ sino + " es bisiesto.");
	}
}

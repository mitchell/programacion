package ejemplos;

public class AmbitoVariables {
	static int variableGlobal;

	public static void main(String[] args) {
		int variableDelMain = 10;
		/*
		 * Aquí se pueden usar variableGlobal y variableDelMain. No se puede usar
		 * variableDeOtroMetodo
		 */
		System.out.println("variableGlobal " + variableGlobal);
		System.out.println("variableDelMain " + variableDelMain);
		otroMetodo();
	}

	static void otroMetodo() {
		int variableDeOtroMetodo = 90;
		/*
		 * Aquí se pueden usar variableGlobal y variableDeOtroMetodo. No se puede usar
		 * variableDelMain
		 */
		System.out.println("variableGlobal " + variableGlobal);
		// System.out.println("variableDelMain " + variableDelMain);
		System.out.println("variableDeOtroMetodo " + variableDeOtroMetodo);
	}

}

package scanner;
import java.util.Scanner;

public class Scanejemplo2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in); // crear un objeto Scanner
		String nombre;
		double radio;
		int n;
		
		System.out.print("Introduzca su nombre: ");
		nombre = sc.nextLine(); // leer un String/linea
		System.out.println("Hola " + nombre + "!!!");
		
		System.out.print("Introduzca el radio de la circunferencia: ");
		//se suele hacer de una vez las siguientes lineas comentadas
		String strRadio = sc.nextLine();
		radio = Double.parseDouble(strRadio); // leer un double
		radio = Double.parseDouble(sc.nextLine());
		
		System.out.println("Longitud de la circunferencia: " + 2 * Math.PI * radio);
		
		sc.nextLine();
		
		System.out.print("Introduzca un número entero: ");
		n = Integer.parseInt(sc.nextLine());
		//n = sc.nextInt(); // leer un entero
		System.out.println("El cuadrado es: " + Math.pow(n, 2));
		sc.close();
	}
}
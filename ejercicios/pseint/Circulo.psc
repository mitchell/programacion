Proceso Circulo
	Definir radio, superficie, perimetro como Real;
	Escribir "introduce el radio de la circunferencia:";
	Leer radio;
	superficie<-PI*radio^2;
	perimetro <-2*PI*radio;
	Escribir "La superficie es ", superficie;
	Escribir "El perímetro es " ,perimetro;
	FinProceso

package ejemplos;

public class Printing {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Mostrar un valor numérico entero 
		System.out.print("Número entero: "); 
		System.out.println(284); 

		//Mostrar un valor numérico real 
		System.out.print("Número real: "); 
		System.out.println(21843.83); 
		//Mostrar un carácter 
		System.out.print("Carácter: "); 
		System.out.println('A'); 

		//Mostrar una cadena de caracteres 
		System.out.print("Cadena de caracteres: "); 
		System.out.println("Saludos a todos"); 

		//Mostrar un valor lógico 
		System.out.print("Valor lógico: "); 
		System.out.println(true); 

	}

}
